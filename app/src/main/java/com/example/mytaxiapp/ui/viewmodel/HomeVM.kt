package com.example.mytaxiapp.ui.viewmodel

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mytaxiapp.util.Resource
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.Exception

class HomeVM : ViewModel() {

    lateinit var context: Context

    private val _state = MutableStateFlow<SearchState>(SearchState())
    val state: StateFlow<SearchState> = _state.asStateFlow()

    private var searchJob: Job? = null

    fun onSearch(point: LatLng)  {

        _state.value.isLoading = true

        searchJob?.cancel()

        searchJob = viewModelScope.launch {
            delay(500L)

            getAddress(point).onEach { result ->
                when (result) {
                    is Resource.Loading -> {
                        _state.value = SearchState(
                            isLoading = true
                        )
                    }
                    is Resource.Success -> {
                        _state.value = SearchState(
                            address = result.data?.get(0)?.getAddressLine(0) ?: "Nothing to show"
                        )
                    }
                    is Resource.Error -> {
                        _state.value = SearchState(
                            error = result.message ?: "Unknown error"
                        )
                    }


                }
            }.launchIn(this)

        }

    }

    private suspend fun getAddress(point: LatLng): Flow<Resource<List<Address>>> = flow {
        emit(Resource.Loading())

        try {

            val geoCoder = Geocoder(context)
            var addresses: List<Address> = ArrayList()
            addresses = geoCoder.getFromLocation(point.latitude, point.longitude, 1)

            emit(Resource.Success(
                data = addresses
            ))

        } catch (e: Exception) {
            emit(Resource.Error(
                message = e.localizedMessage
            ))
        }

    }

}