package com.example.mytaxiapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.mytaxiapp.R
import com.example.mytaxiapp.constants.Constant.POLYLINE_LINE_COLOR
import com.example.mytaxiapp.constants.Constant.POLYLINE_STROKE_WIDTH_PX
import com.example.mytaxiapp.databinding.ScreenTripDetailBinding
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*

class TripDetail : Fragment(),
OnMapReadyCallback{

    private var _binding: ScreenTripDetailBinding? = null
    private val binding get() = _binding!!

    private lateinit var mapFragment: SupportMapFragment
    private lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenTripDetailBinding.inflate(inflater, container, false)

        mapFragment = childFragmentManager.findFragmentById(R.id.mapInDetail) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.buttonBack.setOnClickListener { findNavController().navigateUp() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        val polyline = map.addPolyline(
            PolylineOptions()
                .add(
                    LatLng(-35.016, 143.321),
                    LatLng(-34.747, 145.592),
                    LatLng(-34.364, 147.891),
                    LatLng(-33.501, 150.217),
                    LatLng(-32.306, 149.248),
                    LatLng(-32.491, 147.309)
                )
        )
        stylePolyline(polyline)

//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-33.501, 150.217), 6f))

        val builder: LatLngBounds.Builder = LatLngBounds.Builder()
        builder.include(LatLng(-35.016, 143.321))
        builder.include(LatLng(-34.747, 145.592))
        builder.include(LatLng(-34.364, 147.891))
        builder.include(LatLng(-32.306, 149.248))
        builder.include(LatLng(-35.016, 143.321))
        builder.include(LatLng(-32.491, 147.309))

        val bounds: LatLngBounds = builder.build()
        val width = resources.displayMetrics.widthPixels
        val padding = (width * 0.25).toInt()

        val cameraUpdate: CameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding)

        map.animateCamera(cameraUpdate)


    }

    private fun stylePolyline(polyline: Polyline) {

        val bitmapStartPoint =
            AppCompatResources.getDrawable(requireContext(), R.drawable.ic_dot_blue)?.toBitmap()
        polyline.startCap = CustomCap(
            BitmapDescriptorFactory.fromBitmap(bitmapStartPoint!!), 8f
        )

        val bitmapEndPoint =
            AppCompatResources.getDrawable(requireContext(), R.drawable.ic_dot_red)?.toBitmap()

        polyline.endCap = CustomCap(
            BitmapDescriptorFactory.fromBitmap(bitmapEndPoint!!), 8f
        )
        polyline.width = POLYLINE_STROKE_WIDTH_PX.toFloat()
        polyline.color = POLYLINE_LINE_COLOR
        polyline.jointType = JointType.ROUND
    }


}
