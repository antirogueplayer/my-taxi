package com.example.mytaxiapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.mytaxiapp.R
import com.example.mytaxiapp.ui.adapter.SectionedTripAdapter
import com.example.mytaxiapp.databinding.ScreenMyTripsBinding
import com.example.mytaxiapp.model.SectionedTripData
import com.example.mytaxiapp.model.TripData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MyTrips : Fragment() {

    private var _binding: ScreenMyTripsBinding? = null
    private val binding get() = _binding!!

    private lateinit var list: List<SectionedTripData>

    private val myAdapter: SectionedTripAdapter by lazy {
        SectionedTripAdapter()
    }
    private val navController by lazy {
        findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenMyTripsBinding.inflate(inflater, container, false)

        list = initList()
        myAdapter.submitList(list)
        binding.rvSectioned.adapter = myAdapter

        myAdapter.setItemClickFromSection {

            navController.navigate(
                MyTripsDirections.actionMyTripsToTripDetail(it)
            )


        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.toolbar.setNavigationOnClickListener { navController.navigateUp() }
        binding.shimmerLayout.startShimmer()
        lifecycleScope.launch {
            delay(1000L)
            binding.apply {
                shimmerLayout.stopShimmer()
                shimmerLayout.visibility = View.GONE
                rvSectioned.visibility = View.VISIBLE
            }
        }


    }

    private fun initList(): List<SectionedTripData> =
        listOf(
            SectionedTripData(
                "6 Июля, Вторник", listOf(
                    TripData(
                        12,
                        "Sharof Rashidov",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "10:42",
                        "23 700 cум",
                        R.drawable.im_bm_car
                    ),
                    TripData(
                        114,
                        "Яшнабадский район, улица Sharof Rashidov, Ташкент",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "10:42",
                        "13 000 cум",
                        R.drawable.im_bm_car
                    ),
                    TripData(
                        543,
                        "Яшнабадский район, улица Sharof Rashidov, Ташкент",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "12:54",
                        "15 000 cум",
                        R.drawable.im_bm_car
                    ),

                    )
            ),
            SectionedTripData(
                "5 Июля, Вторник", listOf(
                    TripData(
                        345,
                        "Яшнабадский район, улица Sharof Rashidov, Ташкент",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "10:42",
                        "16 000 cум",
                        R.drawable.im_bm_car
                    )
                )
            ),
            SectionedTripData(
                "3 Июля, Вторник", listOf(
                    TripData(
                        65,
                        "Яшнабадский район, улица Sharof Rashidov, Ташкент",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "10:42",
                        "8 000 cум",
                        R.drawable.im_bm_car
                    ),
                    TripData(
                        76,
                        "Яшнабадский район, улица Sharof Rashidov, Ташкент",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "10:42",
                        "8 000 cум",
                        R.drawable.im_bm_car
                    ),

                    )
            ),
            SectionedTripData(
                "1 Июля, Вторник", listOf(
                    TripData(
                        786,
                        "Яшнабадский район, улица Sharof Rashidov, Ташкент",
                        "Юнусабадский район, м-в юнусабад-19, ул. дехканабад, 17",
                        "10:42",
                        "11 000 cум",
                        R.drawable.im_bm_car
                    )
                )
            )

        )

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}