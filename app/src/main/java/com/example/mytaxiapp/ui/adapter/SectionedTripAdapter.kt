package com.example.mytaxiapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mytaxiapp.databinding.ItemSectionedTripBinding
import com.example.mytaxiapp.model.SectionedTripData
import com.example.mytaxiapp.model.TripData

class SectionedTripAdapter: ListAdapter<SectionedTripData,SectionedTripAdapter.VH>(MyDiffUtil) {

    private var itemClickFromSection: ((TripData) -> Unit)? = null

    object MyDiffUtil: DiffUtil.ItemCallback<SectionedTripData>() {
        override fun areItemsTheSame(
            oldItem: SectionedTripData,
            newItem: SectionedTripData
        ): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(
            oldItem: SectionedTripData,
            newItem: SectionedTripData
        ): Boolean =
            oldItem.section == newItem.section
    }

    inner class VH(val binding: ItemSectionedTripBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = ItemSectionedTripBinding
            .inflate(LayoutInflater.from(parent.context),parent,false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        with(holder.binding) {
            with(getItem(position)) {
                tvSection.text = this.section
                val adapter = TripAdapter()
                adapter.submitList(this.list)
                rvTrips.adapter = adapter

                adapter.itemClickListener {
                    itemClickFromSection?.invoke(it)
                }

            }
        }
    }

    fun setItemClickFromSection(block: (TripData) -> Unit) {
        itemClickFromSection = block
    }



}