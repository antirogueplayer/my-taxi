package com.example.mytaxiapp.ui.viewmodel

data class SearchState (
    var address: String = "",
    var isLoading: Boolean = false,
    var error: String = ""
)