package com.example.mytaxiapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mytaxiapp.databinding.ItemTripBinding
import com.example.mytaxiapp.model.TripData

class TripAdapter: ListAdapter<TripData, TripAdapter.VH>(MyDiffUtil) {

    object MyDiffUtil: DiffUtil.ItemCallback<TripData>() {
        override fun areItemsTheSame(
            oldItem: TripData,
            newItem: TripData
        ): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(
            oldItem: TripData,
            newItem: TripData
        ): Boolean =
            oldItem.orderId == newItem.orderId
    }

    private var itemClick: ((TripData) -> Unit)? = null

    inner class VH(val binding: ItemTripBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = ItemTripBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        with(holder) {
            with(getItem(position)) {
                binding.addressFrom.text = this.addressFrom
                binding.addressTo.text = this.addressTo
                binding.timeArrived.text = this.timeStamp
                binding.costPaid.text = this.fee
                binding.imageCar.setImageResource(this.carType)

                binding.itemTrip.setOnClickListener {
                    itemClick?.invoke(this)
                }

            }
        }
    }

    fun itemClickListener(block: (TripData) -> Unit) {
        itemClick = block
    }

}