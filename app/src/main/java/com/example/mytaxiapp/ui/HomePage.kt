package com.example.mytaxiapp.ui


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.mytaxiapp.R
import com.example.mytaxiapp.constants.Constant.POLYLINE_LINE_COLOR
import com.example.mytaxiapp.databinding.PageHomeBinding
import com.example.mytaxiapp.ui.viewmodel.HomeVM
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.flow.collectLatest


class HomePage : Fragment(),
    GoogleMap.OnCameraMoveListener,
    GoogleMap.OnCameraMoveStartedListener,
    GoogleMap.OnCameraIdleListener,
    GoogleMap.OnCameraMoveCanceledListener,
    OnMapReadyCallback {

    private var _binding: PageHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var mapFragment: SupportMapFragment
    private lateinit var map: GoogleMap

    private val TAG = "TTT"
    private val tashkentLatLng = LatLng(41.311081, 69.240562)

    private var currPolylineOptions: PolylineOptions? = null
    private var isCanceled = false

    private lateinit var viewModel: HomeVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PageHomeBinding.inflate(inflater, container, false)

        checkReadyThen {
            mapFragment =
                childFragmentManager.findFragmentById(R.id.mapInHome) as SupportMapFragment
            mapFragment.getMapAsync(this)
        }

        viewModel = ViewModelProvider(this)[HomeVM::class.java]
        viewModel.context = requireContext()


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        subscribeToFragment()


    }

    private fun subscribeToFragment() {
        lifecycleScope.launchWhenStarted {
            viewModel.state.collectLatest {
                if (it.isLoading) binding.pinAddress.text = "Loading..."
                binding.pinAddress.text = it.address
                binding.tvAddressTo.text = it.address ?: "Searching..."
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {

        checkReadyThen {
            map = googleMap


            with(googleMap) {

                setMinZoomPreference(8f)
                setMaxZoomPreference(20f)

                setOnCameraIdleListener(this@HomePage)
                setOnCameraMoveListener(this@HomePage)
                setOnCameraMoveStartedListener(this@HomePage)
                setOnCameraMoveCanceledListener(this@HomePage)

                uiSettings.isZoomControlsEnabled = false
                uiSettings.isMyLocationButtonEnabled = false

                moveCamera(CameraUpdateFactory.newLatLngZoom(tashkentLatLng, 10f))

            }
        }


    }

    private fun checkReadyThen(stuffToDo: () -> Unit) {
        if (::map.isInitialized) {
            Toast.makeText(requireContext(), "Map is ready to go", Toast.LENGTH_SHORT).show()
        } else {
            stuffToDo()
        }
    }


    override fun onCameraMoveStarted(reason: Int) {
        if (!isCanceled) map.clear()

        currPolylineOptions = PolylineOptions().width(5f)
        currPolylineOptions?.color(POLYLINE_LINE_COLOR)

        addCameraTargetToPath()

    }

    private fun checkPolyLineThen(stuffToDo: () -> Unit) {
        if (currPolylineOptions != null) stuffToDo()
    }

    override fun onCameraMove() {
        checkPolyLineThen { addCameraTargetToPath() }
    }

    override fun onCameraMoveCanceled() {
        checkPolyLineThen {
            addCameraTargetToPath()
            map.addPolyline(currPolylineOptions!!)
        }

        isCanceled = true
        currPolylineOptions = null
        Log.e(TAG, "onCameraMoveCanceled")

    }

    override fun onCameraIdle() {

        checkPolyLineThen {
            addCameraTargetToPath()
            val bitmapEndPoint =
                AppCompatResources.getDrawable(requireContext(), R.drawable.ic_light_red_pin)
                    ?.toBitmap()
            map.addMarker(
                MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromBitmap(bitmapEndPoint!!))
                    .position(map.cameraPosition.target)
                    .snippet("Lat: ${map.cameraPosition.target.latitude}\nLong: ${map.cameraPosition.target.longitude} ")
            )
            map.addPolyline(currPolylineOptions!!)

            viewModel.onSearch(map.cameraPosition.target)

        }


    }

    private fun addCameraTargetToPath() {
        currPolylineOptions?.add(map.cameraPosition.target)
    }


}