package com.example.mytaxiapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.mytaxiapp.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val navView: NavigationView = binding.navigationView
        val navController = findNavController(R.id.fragmentContainerView)
        navView.setupWithNavController(navController)

        val navDrawer: DrawerLayout = binding.drawerLayout
        val buttonHamburgerMenu: AppCompatImageButton = binding.buttonOpenDrawer

        setIosAnimationToDrawer()

        buttonHamburgerMenu.setOnClickListener {
            if (!navDrawer.isDrawerOpen(GravityCompat.START)) navDrawer.openDrawer(GravityCompat.START);
            else navDrawer.closeDrawer(GravityCompat.END)
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.homePage -> {
                    buttonHamburgerMenu.visibility = View.VISIBLE
                    unLockDrawer()
                }
                else -> {
                    buttonHamburgerMenu.visibility = View.GONE
                    lockDrawer()
                }
            }
        }


        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.myTrips -> {
                    navController.navigate(R.id.myTrips)
                    true
                }

                else -> false

            }

        }


    }

    private fun lockDrawer() {
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    private fun unLockDrawer() {
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    private fun setIosAnimationToDrawer() {
        binding.drawerLayout.apply {
            elevation = 0f
            setScrimColor(Color.TRANSPARENT)
            addDrawerListener(object : DrawerLayout.DrawerListener {
                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    val slideX = drawerView.width * slideOffset
                    binding.contentHolder.apply {
                        translationX = slideX
                        scaleY = 1 - (slideOffset / 8f)
                        scaleX = 1 - (slideOffset / 8f)
                    }

                }

                override fun onDrawerOpened(drawerView: View) {
                }

                override fun onDrawerClosed(drawerView: View) {
                }

                override fun onDrawerStateChanged(newState: Int) {
                }
            })
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return true
    }



}