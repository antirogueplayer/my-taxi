package com.example.mytaxiapp.constants

import androidx.annotation.ColorInt

object Constant {

    const val POLYLINE_STROKE_WIDTH_PX = 8
    @ColorInt const val POLYLINE_LINE_COLOR: Int = 0xFF3F7BEB.toInt()

}