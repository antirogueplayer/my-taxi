package com.example.mytaxiapp.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class TripData(
    val orderId: Int,
    val addressFrom: String,
    val addressTo: String,
    val timeStamp: String,
    val fee: String,
    @DrawableRes val carType: Int
): Parcelable
