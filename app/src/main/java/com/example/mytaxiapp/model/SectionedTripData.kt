package com.example.mytaxiapp.model

import com.example.mytaxiapp.model.TripData

data class SectionedTripData(
    val section: String,
    val list: List<TripData>
)
